﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogMVC.Data;

namespace BLogMVC.Business.Businesses
{
    public class BusinessBase<T>: IBusinessBase<T> where T: class
    {
        protected readonly IRepositoryBase<T> _repository;

        public BusinessBase(IRepositoryBase<T> repository)
        {
            _repository = repository;
        }
        public bool Add(T entity)
        {
            return _repository.Add(entity);
        }

        public T GetSingleById(Guid id)
        {
            return _repository.GetSingleById(id);
        }

        public IEnumerable<T> GetAll()
        {
            return _repository.GetAll();
        }

        public bool Update(T entity)
        {
            return _repository.Update(entity);
        }

        public bool Delete(T entity)
        {
            return _repository.Delete(entity);
        }

        public bool Delete(Guid id)
        {
            return _repository.Delete(id);
        }
    }
}
