﻿using BlogMVC.Data;
using BlogMVC.Data.Repositories;
using BlogMVC.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLogMVC.Business.Businesses
{
    public class ArticleBusiness: BusinessBase<Article>
    {
        //private readonly IRepositoryBase<Article> _articleRepository;

        public ArticleBusiness(IRepositoryBase<Article> articleRepository): base(articleRepository)
        {
            //_articleRepository = articleRepository;
        }


    }
}
