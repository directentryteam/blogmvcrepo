﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLogMVC.Business
{
    public interface IBusinessBase<T> where T: class
    {
        /// <summary>
        /// add new entity to database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Add(T entity);

        /// <summary>
        /// get entity using id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetSingleById(Guid id);

        /// <summary>
        /// get all entity in database
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// update entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Update(T entity);

        /// <summary>
        /// delete entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Delete(T entity);

        /// <summary>
        /// delete entity using id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(Guid id); 
    }
}
