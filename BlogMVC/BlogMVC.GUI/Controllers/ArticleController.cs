﻿using BlogMVC.Model.Models;
using BLogMVC.Business;
using BLogMVC.Business.Businesses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace BlogMVC.GUI.Controllers
{
    public class ArticleController : Controller
    {
        private readonly IBusinessBase<Article> _articleBusiness;
        
        public ArticleController(IBusinessBase<Article> articleBusiness)
        {
            _articleBusiness = articleBusiness;
        }

        public ActionResult Index()
        {
            var list = new List<Article>();
            list = _articleBusiness.GetAll().ToList();
            ICollection<int> c = new HashSet<int>();
            var article = list[0];
            return View(article);
        }

        public ActionResult GetAllArticle()
        {
            var list = new List<Article>();
            list = _articleBusiness.GetAll().ToList();
            return View(list);
        }
	}
}