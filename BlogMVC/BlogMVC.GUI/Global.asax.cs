﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using BlogMVC.Data;
using System.Reflection;
using BLogMVC.Business.Businesses;
using BlogMVC.Data.Repositories;

namespace BlogMVC.GUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterAssemblyTypes(typeof(BusinessBase<>).Assembly)
                .Where(t => t.Name.EndsWith("Business"))
                .AsImplementedInterfaces().InstancePerLifetimeScope().PreserveExistingDefaults();

            builder.RegisterAssemblyTypes(typeof(RepositoryBase<>).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().SingleInstance().PreserveExistingDefaults();

            //builder.RegisterAssemblyTypes(typeof(BlogDbContext).Assembly)
            //    .AsImplementedInterfaces().SingleInstance();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
