﻿using BlogMVC.Model.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogMVC.Model.Configurations
{
    public class ArticleConfiguration: EntityTypeConfiguration<Article>
    {
        public ArticleConfiguration()
        {
            Property(d => d.ArticleId).IsRequired().HasColumnName("ID");
            HasKey(d => d.ArticleId);
            Property(d => d.Content).IsRequired();
            Property(d => d.Content).IsUnicode(true);
            ToTable("Article");
            
        }
    }
}
