﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogMVC.Model.Models
{
    public class Trip
    {
        public int TripId { get; set; }
        public virtual ICollection<Activity> Activities{ get; set; }
    }
}
