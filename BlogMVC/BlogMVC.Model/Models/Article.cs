﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogMVC.Model.Models
{
    [Table(name:"Article")]
    public class Article
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ArticleId { get; set; }
        public string Title { get; set; }
        public Guid AuthorId { get; set; }
        [ForeignKey("Category")]
        public Guid CategoryId { get; set; }
        public DateTime Posted { get; set; }
        public string Content { get; set; }
        public bool Active { get; set; }
        public bool IsActive { get; set; }

        public virtual Category Category { get; set; }

        public Article()
        {
            Active = true;
        }


    }
}
