﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogMVC.Model.Models
{
    [Table(name:"Category")]
    public class Category
    {
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }

        public virtual ICollection<Article> Articles { get; set; }

        public Category()
        {
            Articles = new HashSet<Article>();
        }
    }
}
