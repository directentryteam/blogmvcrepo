﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogMVC.Model.Models
{
    public class Activity
    {
        public int ActivityId { get; set; }
        public virtual ICollection<Trip> Trips { get; set; }
    }
}
