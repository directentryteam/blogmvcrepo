﻿using System.Data.Entity;
using BlogMVC.Model.Models;
using BlogMVC.Model.Configurations;

namespace BlogMVC.Data
{
    public class BlogDbContext: DbContext
    {
        public BlogDbContext(): base("name=BlogDb")
        {

        }

        public DbSet<Article> Articles { get; set; }
        public DbSet<Category> Categories { get; set; }

        //

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<BlogDbContext>());
            modelBuilder.Configurations.Add(new ArticleConfiguration());
            modelBuilder.Entity<Article>().HasRequired(d => d.Category);
            //modelBuilder.Entity<Category>().HasRequired(d => d.Articles).WithMany().WillCascadeOnDelete(true);
        }
    }
}
