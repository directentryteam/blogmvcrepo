namespace BlogMVC.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class columnchange : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Article",
                c => new
                    {
                        ID = c.Guid(nullable: false, identity: true),
                        Title = c.String(),
                        AuthorId = c.Guid(nullable: false),
                        CategoryId = c.Guid(nullable: false),
                        Posted = c.DateTime(nullable: false),
                        Content = c.String(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Category", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        CategoryId = c.Guid(nullable: false),
                        CategoryName = c.String(),
                    })
                .PrimaryKey(t => t.CategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Article", "CategoryId", "dbo.Category");
            DropIndex("dbo.Article", new[] { "CategoryId" });
            DropTable("dbo.Category");
            DropTable("dbo.Article");
        }
    }
}
