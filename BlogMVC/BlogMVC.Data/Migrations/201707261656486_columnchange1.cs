namespace BlogMVC.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class columnchange1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Article", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Article", "IsActive");
        }
    }
}
