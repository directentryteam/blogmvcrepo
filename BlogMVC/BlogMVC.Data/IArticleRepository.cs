﻿using BlogMVC.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogMVC.Data
{
    public interface IArticleRepository: IRepositoryBase<Article>
    {

    }
}
