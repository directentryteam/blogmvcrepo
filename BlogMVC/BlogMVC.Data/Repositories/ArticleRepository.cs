﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogMVC.Model.Models;

namespace BlogMVC.Data.Repositories
{
    public class ArticleRepository : IArticleRepository
    {
        public bool Add(Article entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Article entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Article> GetAll()
        {
            Category category = new Category
            {
                CategoryId = Guid.NewGuid(),
                CategoryName = "Cat 1"
            };
            var list = new List<Article>();
            list.Add(new Article
            {
                Content = "1234567",
                Title = "Hai",
                Posted = DateTime.Now,
                AuthorId = Guid.NewGuid(),
                CategoryId = category.CategoryId
               
            });
            using (BlogDbContext ctx = new BlogDbContext())
            {
                ctx.Categories.Add(category);
                ctx.Articles.Add(list[0]);
                ctx.SaveChanges();
            }
            return list;
        }

        public Article GetSingleById(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool Update(Article entity)
        {
            throw new NotImplementedException();
        }
    }
}
