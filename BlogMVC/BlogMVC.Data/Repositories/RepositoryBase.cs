﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogMVC.Data.Repositories
{
    public class RepositoryBase<T>: IRepositoryBase<T> where T: class
    {
        protected readonly IRepositoryBase<T> _repositoryBase;
        protected BlogDbContext dataContext;
        protected BlogDbContext DataContext
        {
            get
            {
                return dataContext ?? (dataContext = new BlogDbContext());
            }
        }

        public RepositoryBase(IRepositoryBase<T> repositoryBase)
        {
            _repositoryBase = repositoryBase;
        }

        public bool Add(T entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public virtual IEnumerable<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public T GetSingleById(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool Update(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
